---
layout: handbook-page-toc
title: FY22-Q2 Learning & Development Newsletter
---

<figure class="video_container">
  <iframe width="560" height="315" src="https://www.youtube.com/embed/YSk10qIFUao" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

Welcome to the GitLab Learning & Development (L&D) newsletter! The purpose of the L&D newsletter is to enable a culture of curiosity and continuous learning that *prioritizes learning* with a [growth mindset](/handbook/values/#growth-mindset) for team members. The quarterly newsletter will raise awareness of what learning initiatives took place in the past quarter, insight into what's coming next, learning tips, and encourage participation. We will also feature leadership and learner profiles that highlight what our community has done to learn new skills. Consider this a forum to hear from others across GitLab on what learning has done for them. 

You can find more information on the [structure and process](/handbook/people-group/learning-and-development/newsletter/) for the L&D newsletter, as well as links to [past L&D Newsletters](/handbook/people-group/learning-and-development/newsletter/#past-newsletters). 

## Learn from Leadership 

This quarter we are learning from [Christie Lenneville](/company/team/#clenneville). 

<figure class="video_container">
  <iframe width="560" height="315" src="https://www.youtube.com/embed/ZSc4UnzqN1Y" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

Check out Christie's favorite learning resource from the past year - [Executive Leadership LinkedIn Learning Course](https://www.linkedin.com/learning/executive-leadership/take-action-to-lead?u=2255073).  

**Is there a leader at GitLab that you want to learn more about?** To nominate someone for our Learn from Leadership section in our next newsletter, use [this nomination form](https://docs.google.com/forms/d/e/1FAIpQLSeuOIH2r_gaQlv6woW96_8BfjBUbzWxLuxoZA7TW-MXz7cT0g/viewform). 

## Learner Spotlight   

Our Learner Spotlight for this quarter is [Viktor Nagy](https://about.gitlab.com/company/team/#nagyv-gitlab). We would like to thank [Kevin Chu](https://about.gitlab.com/company/team/#kbychu) for nominating Viktor! Continue reading to learn more about Viktor and how he prioritizes learning.  

* **What is your current role at GitLab?** 
   * _Viktor_: Senior Product Manager, Configure
* **How long have you been a GitLab team member?** 
   * _Viktor_: Since 2019-09-02
* **How do you make time for learning in your normal day-to-day?** 
   * _Viktor_: I am up for learning almost all day and if a learning opportunity finds me, I try to grab it. Moreover, living in Europe allows me to start work around 10 and only rarely have meetings before 13:00. As a result, when I have a longer term, more demanding learning project (let it be a book or a course), I can easily set up time for it in my morning and early work hours.
* **Why do you make learning a priority?** 
   * _Viktor_: Why not? To me learning is not about the profession or about career, but about what I want. Thankfully, I love my job a lot, as a result, I don’t mind learning related things either, but I really approach the topic from the point of view of my basic motivations and aspirations. I learn because I want something. I want to build a better world.
* **What is your preferred learning style? (Different learning styles - Visual: You prefer using pictures, images, and spatial understanding. Auditory: You prefer using sound and music. Kinesthetic: You prefer using your body, hands and sense of touch. Reading/Writing: You prefer using words, both in speech and writing.)** 
   * _Viktor_: I have learned that the brain is actually a locomotor organ, and the best way to develop our brain is together with movement. Sometime this is hard (“how to add movement to the jobs to be done theory?”), but even then I can take a walk, and discuss the topic with someone. Beyond that I try to speak/write about everything that I have learned or even during the process, this allows me to think through and understand the topic much better, and I can get feedback about my understanding and opinion too. Finally, as I approach learning from my personal motivations, I try to put everything into practice. The most exciting part of this is making philosophy a practice!
* **Have you taken advantage of GitLab’s [Growth & Development Benefit](/handbook/total-rewards/benefits/general-and-entity-benefits/growth-and-development/)? If so, what program/course have you completed or what conference(s) have you attended?** 
   * _Viktor_: Yes, multiple times. I took part in product management related training, got reimbursed on book costs, and even hired an English teacher to improve my grammar.
* **Have you recently completed a course? If so, which course (or courses) did you complete and why?** 
   * _Viktor_: I’m currently participating in a [Game Theory course from Coursera](https://www.coursera.org/learn/game-theory-1/home/welcome). I have learned advanced Game Theory during my PhD, and this is actually just a refresher for me. I have a theory that I would like to model properly to be able to publish it in a scientific journal, and I need to polish my game theory and math knowledge for that. 
* **Do you have a favorite or recommended course for others?** 
   * _Viktor_: Only in Hungarian. I think that learning always starts with oneself, and the best way to avoid all the crap in this realm is to focus on the amazing wisdom of philosophy. There is an outstanding Hungarian research institute with philosophy classes that I would recommend to everyone, Fontanus Központ.
* **What advice would you give someone who is struggling to make learning a priority?** 
   * _Viktor_: Ask yourself what do you want? Then check if your daily routine is aligned with your aspirations, and fix your life. Learning might be part of it or not.
* **Anything else you would like to share about learning?**
   * _Viktor_: One of the biggest philosophers I know about is Socrates, from Ancient Greece, apparently he said that “an unexamined life is not worth living”. Learning provides the tools to examine our lives.

**Do you know a team member that places an emphasis on learning?** To nominate someone for our Learner Spotlight for our next newsletter, use [this nomination form](https://docs.google.com/forms/d/e/1FAIpQLSfi72ONbp8UcUXDCL__TPAoCEEGH4K_9i1-ZQN7yh_YzlVx0w/viewform). 

## Department Spotlight 

This quarter's team spotlight is on the [Team Member Relations](/handbook/people-group/#team-member-relations-specialist) Team. 

- **What is the goal of the Team Member Relations team?** 

   - Team Member Relations provides all Team Members with a safe environment to discuss workplace issues.  Our main goal is to strengthen relationships and collaboration through identifying and resolving issues, and providing support, coaching and consultation during performance management/improvement efforts and interpersonal conflicts.  

- **Can you run through the team & what different functions do?** 

   - Currently, Team Member Relations consists of Amy Tisdale who supports Team Members in North America and David Cives who supports Team Members in EMEA and APAC.  
  
- **When should/do Team Members reach out to Team Member Relations?** 

   - Team Members can reach out with questions related to coaching, performance management/improvement or interpersonal conflict.  The sooner we know about the concern, the sooner we can help. To learn about how to engage with the team, please review [“Discussing Private Concerns”](/handbook/people-group/#discussing-private-concerns). If you prefer to remain anonymous, you can instead report any concerns via [Lighthouse](/handbook/people-group/#how-to-report-violations). 

- **When should/do people leaders reach out to Team Member Relations?** 

   - People Leaders may reach out to Team Member Relations to receive coaching and guidance regarding a performance, conduct, or interpersonal concern.  Team Member Relations will discuss the situation and recommend solutions and next steps.   

   - When in doubt, reach out.  Before the concern escalates, talk it through with your Team Member Relations Specialist.  

- **Important handbook pages relating to Team Member Relations**

   - Please review the [Team Member Relations Specialist](/handbook/people-group/#team-member-relations-specialist) portion of the handbook.  

- **Why is it important to reach out to Team Member Relations?**

   - Resolving concerns quickly and proactively helps GitLab remain an employer of choice and a success in the marketplace.  

**Is there a team at GitLab you want to learn more about?** Each quarter we will feature a different team and what they do here at GitLab. Leave a comment on the [dicussion issue] if there is a team you would like to see featured or if you would like your team to be featured! 

## Upcoming in FY22-Q2 

Our L&D team will have a variety of learning initiatives throughout the quarter to help reinforce our culture of learning. 

* Crucial Conversations 
   * Samatha will train as a Crucial Conversations trainer 
   * We plan to deliver the training to one group this quarter 
* [Skill of the Month](/handbook/people-group/learning-and-development/learning-initiatives/#skill-of-the-month) 
   * May - Effective Communication
   * June - Coaching 
   * July - Culture of Feedback 
* [Manager Challenge Program (May)](/handbook/people-group/learning-and-development/manager-challenge/)
* [Field Manager Development Program](/handbook/sales/field-manager-development/)
* [Mental Health Newsletter](/handbook/people-group/learning-and-development/newsletter/mental-health-newsletter/FY22-Q1/)
* [Develop Learning Hubs in GitLab Learn](https://about.gitlab.com/handbook/people-group/learning-and-development/work-with-us/#creating-a-learning-hub-for-your-team)
* Planning a [Women at GitLab Mentorship program](https://gitlab.com/gitlab-com/people-group/learning-development/general/-/issues/122) as a second iteration to the Women in Sales Mentorship Pilot. 
* [Learning speaker series](https://about.gitlab.com/handbook/people-group/learning-and-development/learning-initiatives/#learning-speaker-series-overview) on [manager enablement of mental wellness and building a rest ethic with John Fitch](https://www.google.com/url?q=https://docs.google.com/document/d/1E766piNneGCLod4LdT3_8wGd_i0Bj2w8T8BXCWbsqM8/edit&sa=D&source=calendar&ust=1620568745675000&usg=AOvVaw16iuM0GgUClxZNkOyLXJMD).

**Note:** More learning activities may be added throughout the quarter. To stay up to date, please join our [#learninganddevelopment](https://app.slack.com/client/T02592416/CMRAWQ97W) Slack Channel and follow the [GitLab Learning & Development](https://gitlab.edcast.com/channel/gitlab-learning-development) channel in GitLab Learn. 

## Recap of FY22-Q1

FY22-Q1 was full of Learning Initiatives by the Learning & Development Team. If you missed them or want to go back and review anything, here is an outline of what took place: 

* Launched our first [LXP and LinkedIn Learning survey](https://gitlab.com/gitlab-com/people-group/learning-development/general/-/issues/205)
* [Bias Towards Asynchronous Communication](https://gitlab.edcast.com/pathways/ECL-d28057c0-d024-41b0-a89d-9e4d5a024932) self-paced learning path launched in GitLab Learn
* Jacie trained as Crucial Conversation trainer, Samantha completed Crucial Conversations certification
   * If you are interested in completing the Crucial Conversations training, please fill out [this interest form](https://docs.google.com/forms/d/e/1FAIpQLSdqwibbQZs-zL-IX9aq9Yzgozm-y3i0Vwh59T8T1nR74mxmFQ/viewform)
* Collaborated with Sales Enablement on [Interview Training](https://gitlab.com/gitlab-com/people-group/learning-development/training-curriculum/-/issues/3) 
* Shared [internal Rest Ethic and mental wellness training campaign](https://gitlab.com/gitlab-com/people-group/learning-development/general/-/issues/198)
* Launched [Skill of the Month](https://about.gitlab.com/handbook/people-group/learning-and-development/learning-initiatives/#skill-of-the-month)
* Launched [Walk & Learn](https://about.gitlab.com/handbook/people-group/learning-and-development/linkedin-learning/#walk-and-learn)
* Created 2 new Job Families for our team! 
   * [Instructional Designer](https://about.gitlab.com/job-families/people-ops/instructional-designer/)
   * [Learning System Administrator](https://about.gitlab.com/job-families/people-ops/learning-system-administrator/)
* Mental Health Newsletter {add link when published}
* [LinkedIn Learning Cohorts](https://about.gitlab.com/handbook/people-group/learning-and-development/linkedin-learning/#linkedin-learning-cohorts)

### New and Updated Leadership Pages 

The following videos are examples of the [CEO Handbook Learning Sessions](/handbook/people-group/learning-and-development/learning-initiatives/#ceo-handbook-learning-sessions) the L&D Team is doing to uplevel the Leadership handbook pages. 

<figure class="video_container">
  <iframe width="560" height="315" src="https://www.youtube.com/embed/CHs8NxRSGPw" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

If you want to learn more on this topic, see the [High Output Management](/handbook/leadership/high-output-management/#applying-high-output-management) handbook page. 

<figure class="video_container">
  <iframe width="560" height="315" src="https://www.youtube.com/embed/-mLpytnQtlY" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

If you want to learn more on this topic, see the [Managing Underperformance](/handbook/leadership/underperformance/#managing-underperformance-handbook-learning-session) handbook page. 

### Learning & Development Blog Posts 

* [Building an All-Remote Management Enablement Program](https://about.gitlab.com/blog/2021/02/19/manager-training/) 
* [Having crucial conversations on an all-remote team](https://about.gitlab.com/blog/2021/02/18/crucial-conversations/)

## Learning Spotlight 

Skill of the Month launched in April and will continue thorughout FY22. April's skill of the month was Managing Stress, you can still find the courses and resources recommended for this topic in the [Skill of the Month (FY22) Channel](https://gitlab.edcast.com/channel/skill-of-the-month-fy22) on GitLab Learn. The May skill of the month is **Effective Communication**. 

You can do two things at once by taking a course from the Skill of the Month recomendation and participate in the Walk and Learn initiative. If you aren't intersted in any of the courses for skill of the month. Join the [#walk-and-learn](https://app.slack.com/client/T02592416/C01T24NNZ4G) Slack channel to see what other things your peers are learning about. 

## Learning Tips 

We want to remind you to [take time out to learn](/handbook/people-group/learning-and-development/#take-time-out-to-learn-campaign)! [Focus Fridays](/handbook/communication/#focus-fridays) have been extended **indefinitely** and are a great time to focus on learning. Consider blocking off a few hours each week on Fridays to learn new skills for your role at GitLab. 

There are plenty of courses on GitLab Learn as well as LinkedIn Learning that you can access. Check out our [recommended LinkedIn Learning courses](/handbook/people-group/learning-and-development/linkedin-learning/)! If you find a course that you think should be on the recommendation list, make an MR to the list and ask a L&D Team member in the `#learninganddevelopment` Slack Channel to merge. 

## Other Enablement Initiatives

* Check out the monthly [Field Flash Newsletter](/handbook/sales/field-communications/field-flash-newsletter/#past-newsletters)
* The Diversity, Inclusion, & Belonging Team has a newsletter. Search your inbox for the subject line `DIB - Diversity, Inclusion and Belonging Quarterly Newsletter`. 

## Discussion 

If you would like to discuss items related to this newsletter, please see the [related discussion issue](https://gitlab.com/gitlab-com/people-group/learning-development/newsletter/-/issues/10).
